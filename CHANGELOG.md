# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased (0.1b2019051001)]
### Added
- Mendapatkan Daftar Paket Tender dan Non Tender
- Mendapatkan Detil Paket (Pengumuman, Peserta, Hasil Evaluasi, Pemenang, Pemenang Berkontrak, Jadwal Penetapan Pemenang, Jadwal Penandatangan Kontrak)
- Filter pencarian paket tender/non tender berdasarkan kategori pengadaan.
- Mengurutkan Pencarian paket berdasarkan id paket, nama instansi, tahap paket, dan HPS
- Menambahkan CLI downloader paket tender dan non tender

[Unreleased]: https://gitlab.com/wakataw/pyproc/tags/v0.1b2019051001
